# Disable/Delete Linux Accounts

Using Ansible to disable/delete system accounts that are no longer required. for auditing purposes using Ansible [facts](https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html) and interactive prompts when the playbook is ran.

#### Components

Each playbook carries out similar tasks to easily disable or delete users interactively whth the following differences
- [disable_linux_user.yml](disable_linux_user.yml)  - expires account to  January 1st 2010 00:00.01
- [delete_linux_user.yml](delete_linux_user.yml) -  removes account and purges home directory
